<?php
include_once __DIR__ . '/../../Model/Mahasiswa.php';
$nim = $_REQUEST['nim'];
$mhs = Mahasiswa::getByPrimaryKey($nim);
if ($mhs === null) {
    echo "<h2>Data Mahasiswa tidak ditemukan</h2>";
    echo "<a href='index.php'>Klik Link Ini untuk kembali</a>";
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Ubah Mahasiswa</title>
</head>

<body>
    <h2>Ubah Mahasiswa</h2>
    <form method="POST" action="prosesUbah.php">
        <p>Nim <br>
            <input required readonly value="<?= $mhs->nim ?>" type="text" name="nim" />
        </p>
        <p>Nama <br> <input required value="<?= $mhs->nama ?>" type="text" name="nama" /></p>
        <p>Tanggal Lahir <br>
            <input required value="<?= $mhs->tanggalLahir ?>" type="date" name="tanggalLahir" />
        </p>
        <p>Alamat <br> <input required value="<?= $mhs->alamat ?>" type="text" name="alamat" /></p>
        <p>Jenis Kelamin <br>
            <input required <?= $mhs->jenisKelamin == 'L' ? 'checked' : '' ?> type="radio" name="jenisKelamin" value="L" /> Laki Laki
            <input required <?= $mhs->jenisKelamin == 'P' ? 'checked' : '' ?> type="radio" name="jenisKelamin" value="P" /> Perempuan
        </p>
        <button type="submit">Simpan</button>
    </form>
</body>

</html>