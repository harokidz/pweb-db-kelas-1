<?php
include_once __DIR__ . '/../../Model/Mahasiswa.php';
$listMahasiswa = Mahasiswa::getAll();
?>



<div class="card">
    <div class="card-header">
        <h3>List Data Mahasiswa</h3>
    </div>
    <div class="card-body">
        <table id="table-mhs" class="table table-stripped table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>Jumlah Motor</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $nomer = 1;
                foreach ($listMahasiswa as $mhs) {
                ?>
                    <tr>
                        <td><?= $nomer++ ?></td>
                        <td><?= $mhs->nim ?></td>
                        <td><?= $mhs->nama ?></td>
                        <td><?= $mhs->tanggalLahir ?></td>
                        <td><?= $mhs->jenisKelamin ?></td>
                        <td><?= $mhs->alamat ?></td>
                        <td>
                            Memiliki <?= count($mhs->motors) ?> Sepeda Motor : <br>
                            <?php
                            foreach ($mhs->motors as $motor) {
                                echo "$motor->merek $motor->tipe ($motor->platNo) <br>";
                            }
                            ?>
                        </td>
                        <td>
                            <a class="btn btn-warning btn-sm" href="index.php?page=update-mhs&nim=<?= $mhs->nim ?>">Edit</a>
                            <a data-nim='<?= $mhs->nim ?>' class="btn btn-danger btn-sm btn-hapus" href="#">Delete</a>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(function() {
        $('#table-mhs').DataTable();
        $('.btn-hapus').click(function() {
            var nim = $(this).data('nim');
            $.confirm({
                title: 'Konfirmasi Hapus!',
                content: 'Anda Yakin Hapus data ini NIM : ' + nim + '?',
                buttons: {

                    cancel: function() {

                    },
                    somethingElse: {
                        text: 'Hapus',
                        btnClass: 'btn-red',
                        action: function() {
                            $.LoadingOverlay("show");
                            window.location.href = "view/mahasiswa/prosesHapus.php?nim=" + nim;
                        }
                    }
                }
            });
        });
    });
</script>